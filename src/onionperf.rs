mod parse;
mod tgen_stream;
mod tor_circuit;
mod measurement;
mod tor_guard;
mod tor_stream;

use anyhow::{Result};
use data_encoding::HEXUPPER;

use parse::Parse;
use ring::digest::{Context as RingContext, Digest, SHA256};
use serde::Deserialize;
use serde_json::Value;
use std::{
    fs::File,
    fs,
    io::BufReader,
    io::Read,
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH, Duration},
};
use std::collections::HashMap;

use tgen_stream::TgenStream;

use tokio_postgres::{NoTls, types::ToSql};

use tor_circuit::TorCircuit;
use crate::onionperf::measurement::Measurement;
use crate::onionperf::tor_guard::TorGuard;
use crate::onionperf::tor_stream::TorStream;


#[derive(Deserialize)]
struct Config {
    database: DatabaseConfig,
}

#[derive(Deserialize)]
struct DatabaseConfig {
    host: String,
    dbname: String,
    user: String,
    password: String,
    connect_timeout: i32,
    keepalives: i32,
}

fn sha256_digest<R: Read>(mut reader: R) -> Result<Digest> {
    let mut context = RingContext::new(&SHA256);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    Ok(context.finish())
}


fn get_analysis_published(path: &str) -> SystemTime {
    let sys_time = fs::metadata(path).unwrap().modified();
    let tm = sys_time.unwrap();
    tm
}

async fn get_analysis_digest(path: &str) -> Result<String> {
    let input = File::open(path)?;
    let reader = BufReader::new(input);
    let digest = sha256_digest(reader)?;

    Ok(HEXUPPER.encode(digest.as_ref()))
}

async fn insert_analysis(path: &str, record: &Parse, digest: &String) -> Result<u64, tokio_postgres::Error> {
    let client = create_db_connection().await.unwrap();
    let published = get_analysis_published(path);
    let node_id = &record.node_id;
    let measurement_ip = &record.measurement_ip;
    let version = &record.version;
    let op_type = &record.op_type;
    let filters = &record.filters;

    let result = client.execute(
        r#"INSERT INTO onionperf_analysis
        (onionperf_node, published, measurement_ip, type, version, filters, digest) VALUES
        ($1, $2, $3, $4, $5, $6, $7)"#,
        &[node_id, &published, &measurement_ip, &op_type, &version, &filters, &digest],
    ).await?;

    Ok(result)
}


async fn insert_values_batch(
    table: &str,
    columns: Vec<&str>,
    values: Vec<Vec<Arc<dyn ToSql + Sync>>>,
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let client = create_db_connection().await.unwrap();

    let mut results = Vec::new();

    for value_row in values {
        let query = format!(
            "INSERT INTO {} ({}) VALUES ({})",
            table,
            columns.join(", "),
            value_row
                .iter()
                .enumerate()
                .map(|(i, _)| format!("${}", i + 1))
                .collect::<Vec<String>>()
                .join(", ")
        );

        let flattened_values_ref: Vec<&(dyn ToSql + Sync)> =
            value_row.iter().map(Arc::as_ref).collect();

        let result = client.execute(&query, &flattened_values_ref).await?;

        results.push(result);
    }

    Ok(results)
}

// Function to add a single stream to the batch
fn add_stream_to_batch(
    values: &mut Vec<Arc<dyn ToSql + Sync>>,
    node_id: String,
    digest: String,
    st: TgenStream,
) {
    let unix_ts_end = UNIX_EPOCH + Duration::from_secs(st.unix_ts_end as u64);
    let unix_ts_start = UNIX_EPOCH + Duration::from_secs(st.unix_ts_start as u64);

    // Convert node_id and digest into Arc<dyn ToSql + Sync>
    let node_id_arc = Arc::new(node_id) as Arc<dyn ToSql + Sync>;
    let digest_arc = Arc::new(digest) as Arc<dyn ToSql + Sync>;
    let stream_id_arc = Arc::new(st.stream_id) as Arc<dyn ToSql + Sync>;

    // Define a macro to create Arc<dyn ToSql + Sync> for each field
    macro_rules! arc_field {
        ($field:expr) => {
            Arc::new($field) as Arc<dyn ToSql + Sync>
        };
    }

    values.extend_from_slice(&[
        node_id_arc,
        stream_id_arc,
        arc_field!(st.byte_info_total_bytes_recv),
        arc_field!(st.byte_info_payload_bytes_send),
        arc_field!(st.byte_info_payload_progress_recv),
        arc_field!(st.byte_info_payload_progress_send),
        arc_field!(st.byte_info_total_bytes_recv),
        arc_field!(st.byte_info_total_bytes_send),
        arc_field!(st.elapsed_seconds_payload_bytes_recv),
        arc_field!(st.elapsed_seconds_payload_bytes_send),
        arc_field!(st.elapsed_seconds_payload_progress_recv),
        arc_field!(st.elapsed_seconds_payload_progress_send),
        arc_field!(st.is_complete),
        arc_field!(st.is_error),
        arc_field!(st.is_success),
        arc_field!(st.stream_info_error),
        arc_field!(st.stream_info_id),
        arc_field!(st.stream_info_name),
        arc_field!(st.stream_info_peername),
        arc_field!(st.stream_info_recvsize),
        arc_field!(st.stream_info_recvstate),
        arc_field!(st.stream_info_sendsize),
        arc_field!(st.stream_info_sendstate),
        arc_field!(st.stream_info_vertexid),
        arc_field!(st.time_info_created_ts),
        arc_field!(st.time_info_now_ts),
        arc_field!(st.time_info_usecs_to_checksum_recv),
        arc_field!(st.time_info_usecs_to_checksum_send),
        arc_field!(st.time_info_usecs_to_command),
        arc_field!(st.time_info_usecs_to_first_byte_recv),
        arc_field!(st.time_info_usecs_to_first_byte_send),
        arc_field!(st.time_info_usecs_to_last_byte_recv),
        arc_field!(st.time_info_usecs_to_last_byte_send),
        arc_field!(st.time_info_usecs_to_proxy_choice),
        arc_field!(st.time_info_usecs_to_proxy_init),
        arc_field!(st.time_info_usecs_to_proxy_request),
        arc_field!(st.time_info_usecs_to_proxy_response),
        arc_field!(st.time_info_usecs_to_response),
        arc_field!(st.time_info_usecs_to_socket_connect),
        arc_field!(st.time_info_usecs_to_socket_create),
        arc_field!(st.transport_info_error),
        arc_field!(st.transport_info_fd),
        arc_field!(st.transport_info_local),
        arc_field!(st.transport_info_proxy),
        arc_field!(st.transport_info_remote),
        arc_field!(st.transport_info_state),
        arc_field!(unix_ts_end),
        arc_field!(unix_ts_start),
        digest_arc,
    ]);
}


async fn insert_streams(
    record: &Value,
    node_id: &String,
    digest: &String,
    streams: &[String],
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let mut values: Vec<Vec<Arc<dyn ToSql + Sync>>> = Vec::new();

    for stream_id in streams.iter() {
        let stream_json = &record["data"][node_id]["tgen"]["streams"][stream_id];
        match TgenStream::create(stream_json, stream_id) {
            Ok(st) => {
                let mut row_values: Vec<Arc<dyn ToSql + Sync>> = Vec::new();
                let node_id = node_id.to_string();
                let digest = digest.to_string();
                add_stream_to_batch(&mut row_values, node_id, digest, st);
                values.push(row_values);
            }
            Err(err) => {
                eprintln!("Error creating TgenStream: {}", err);
            }
        }

    }

    let columns = vec![
        "onionperf_node",
        "stream_id",
        "byte_info_payload_bytes_recv",
        "byte_info_payload_bytes_send",
        "byte_info_payload_progress_recv",
        "byte_info_payload_progress_send",
        "byte_info_total_bytes_recv",
        "byte_info_total_bytes_send",
        "elapsed_seconds_payload_bytes_recv",
        "elapsed_seconds_payload_bytes_send",
        "elapsed_seconds_payload_progress_recv",
        "elapsed_seconds_payload_progress_sent",
        "is_complete",
        "is_error",
        "is_success",
        "stream_info_error",
        "stream_info_id",
        "stream_info_name",
        "stream_info_peername",
        "stream_info_recvsize",
        "stream_info_recvstate",
        "stream_info_sendsize",
        "stream_info_sendstate",
        "stream_info_vertexid",
        "time_info_created_ts",
        "time_info_now_ts",
        "time_info_usecs_to_checksum_recv",
        "time_info_usecs_to_checksum_send",
        "time_info_usecs_to_command",
        "time_info_usecs_to_first_byte_recv",
        "time_info_usecs_to_first_byte_send",
        "time_info_usecs_to_last_byte_recv",
        "time_info_usecs_to_last_byte_send",
        "time_info_usecs_to_proxy_choice",
        "time_info_usecs_to_proxy_init",
        "time_info_usecs_to_proxy_request",
        "time_info_usecs_to_proxy_response",
        "time_info_usecs_to_response",
        "time_info_usecs_to_socket_connect",
        "time_info_usecs_to_socket_create",
        "transport_info_error",
        "transport_info_fd",
        "transport_info_local",
        "transport_info_proxy",
        "transport_info_remote",
        "transport_info_state",
        "unix_ts_end",
        "unix_ts_start",
        "onionperf_analysis",
    ];

    insert_values_batch("onionperf_tgen_stream", columns, values).await
}

// Function to add a single circuit to the batch
fn add_circuit_to_batch(
    values: &mut Vec<Arc<dyn ToSql + Sync>>,
    node_id: String,
    digest: String,
    circuit: String,
    tc: TorCircuit,
) {
    let unix_ts_end = UNIX_EPOCH + Duration::from_secs(tc.unix_ts_end as u64);
    let unix_ts_start = UNIX_EPOCH + Duration::from_secs(tc.unix_ts_start as u64);

    // Convert node_id, digest, and circuit into Arc<dyn ToSql + Sync>
    let node_id_arc = Arc::new(node_id) as Arc<dyn ToSql + Sync>;
    let digest_arc = Arc::new(digest) as Arc<dyn ToSql + Sync>;
    let circuit_arc = Arc::new(circuit) as Arc<dyn ToSql + Sync>;

    // Define a macro to create Arc<dyn ToSql + Sync> for each field
    macro_rules! arc_field {
        ($field:expr) => {
            Arc::new($field) as Arc<dyn ToSql + Sync>
        };
    }

    values.extend_from_slice(&[
        node_id_arc,
        circuit_arc,
        arc_field!(tc.build_quantile),
        arc_field!(tc.build_timeout),
        arc_field!(tc.buildtime_seconds),
        arc_field!(tc.cbt_set),
        arc_field!(tc.current_guards),
        arc_field!(tc.elapsed_seconds),
        arc_field!(tc.failure_reason_local),
        arc_field!(tc.filtered_out),
        arc_field!(tc.path),
        Arc::new(unix_ts_end),
        Arc::new(unix_ts_start),
        digest_arc,
    ]);
}

async fn insert_circuits(
    record: &Value,
    node_id: &String,
    digest: &String,
    circuits: &[String],
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let mut values: Vec<Vec<Arc<dyn ToSql + Sync>>> = Vec::new();

    for circuit in circuits.iter() {
        let circuit_json = &record["data"][node_id]["tor"]["circuits"][circuit];
        match TorCircuit::create(circuit_json, node_id) {
            Ok(tc) => {
                let mut row_values: Vec<Arc<dyn ToSql + Sync>> = Vec::new();
                add_circuit_to_batch(&mut row_values, node_id.clone(), digest.to_owned(), circuit.clone(), tc);
                values.push(row_values);
            }
            Err(err) => {
                eprintln!("Error creating TorCircuit: {}", err);
            }
        }
    }

    let columns = vec![
        "onionperf_node",
        "circuit_id",
        "build_quantile",
        "build_timeout",
        "buildtime_seconds",
        "cbt_set",
        "current_guards",
        "elapsed_seconds",
        "failure_reason_local",
        "filtered_out",
        "circuit_path",
        "unix_ts_end",
        "unix_ts_start",
        "onionperf_analysis",
    ];

    insert_values_batch("onionperf_tor_circuit", columns, values).await
}

fn add_guard_to_batch(
    values: &mut Vec<Arc<dyn ToSql + Sync>>,
    node_id: String,
    digest: String,
    tg: TorGuard,
) {
    let dropped_ts = UNIX_EPOCH + Duration::from_secs(tg.dropped_ts as u64);

    // Convert node_id, digest, and circuit into Arc<dyn ToSql + Sync>
    let node_id_arc = Arc::new(node_id) as Arc<dyn ToSql + Sync>;
    let digest_arc = Arc::new(digest) as Arc<dyn ToSql + Sync>;

    // Define a macro to create Arc<dyn ToSql + Sync> for each field
    macro_rules! arc_field {
        ($field:expr) => {
            Arc::new($field) as Arc<dyn ToSql + Sync>
        };
    }

    values.extend_from_slice(&[
        node_id_arc,
        arc_field!(tg.country),
        arc_field!(dropped_ts),
        arc_field!(tg.fingerprint),
        arc_field!(tg.nickname),
        digest_arc,
    ]);
}

async fn insert_tor_guards(
    node_id: &String,
    digest: &String,
    guards: &Value,
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let mut values: Vec<Vec<Arc<dyn ToSql + Sync>>> = Vec::new();

    if let Some(array) = guards.as_array() {
        for guard in array {
            match TorGuard::create(guard) {
                Ok(tg) => {
                    let mut row_values: Vec<Arc<dyn ToSql + Sync>> = Vec::new();
                    add_guard_to_batch(&mut row_values, node_id.clone(), digest.to_owned(), tg);
                    values.push(row_values);
                }
                Err(err) => {
                    eprintln!("Error creating TorGuard: {}", err);
                }
            }
        }
    }

    let columns = vec![
        "onionperf_node",
        "country",
        "dropped_ts",
        "fingerprint",
        "nickname",
        "onionperf_analysis",
    ];

    insert_values_batch("onionperf_tor_guard", columns, values).await
}

// Function to add a single tor stream to the batch
fn add_tor_stream_to_batch(
    values: &mut Vec<Arc<dyn ToSql + Sync>>,
    node_id: String,
    digest: String,
    ts: TorStream,
) {
    let unix_ts_end = UNIX_EPOCH + Duration::from_secs(ts.unix_ts_end as u64);
    let unix_ts_start = UNIX_EPOCH + Duration::from_secs(ts.unix_ts_start as u64);

    // Convert node_id, digest, and circuit into Arc<dyn ToSql + Sync>
    let node_id_arc = Arc::new(node_id) as Arc<dyn ToSql + Sync>;
    let digest_arc = Arc::new(digest) as Arc<dyn ToSql + Sync>;

    // Define a macro to create Arc<dyn ToSql + Sync> for each field
    macro_rules! arc_field {
        ($field:expr) => {
            Arc::new($field) as Arc<dyn ToSql + Sync>
        };
    }

    values.extend_from_slice(&[
        node_id_arc,
        arc_field!(ts.stream_id),
        arc_field!(ts.circuit_id),
        arc_field!(ts.elapsed_seconds),
        arc_field!(ts.source),
        arc_field!(ts.target),
        Arc::new(unix_ts_end),
        Arc::new(unix_ts_start),
        digest_arc,
    ]);
}

async fn insert_tor_streams(
    record: &Value,
    node_id: &String,
    digest: &String,
    streams: &[String],
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let mut values: Vec<Vec<Arc<dyn ToSql + Sync>>> = Vec::new();

    for stream in streams.iter() {
        let stream_json = &record["data"][node_id]["tor"]["streams"][stream];
        match TorStream::create(stream_json) {
            Ok(ts) => {
                let mut row_values: Vec<Arc<dyn ToSql + Sync>> = Vec::new();
                add_tor_stream_to_batch(&mut row_values, node_id.clone(), digest.to_owned(), ts);
                values.push(row_values);
            }
            Err(err) => {
                eprintln!("Error creating TorStream: {}", err);
            }
        }
    }

    let columns = vec![
        "onionperf_node",
        "stream_id",
        "circuit_id",
        "elapsed_seconds",
        "source",
        "target",
        "unix_ts_end",
        "unix_ts_start",
        "onionperf_analysis",
    ];

    insert_values_batch("onionperf_tor_stream", columns, values).await
}

fn add_measurement_to_batch(
    values: &mut Vec<Arc<dyn ToSql + Sync>>,
    node_id: String,
    digest: String,
    m: Measurement,
) {
    let unix_ts_end = UNIX_EPOCH + Duration::from_secs(m.unix_ts_end as u64);
    let unix_ts_start = UNIX_EPOCH + Duration::from_secs(m.unix_ts_start as u64);

    // Convert node_id, digest, and circuit into Arc<dyn ToSql + Sync>
    let node_id_arc = Arc::new(node_id) as Arc<dyn ToSql + Sync>;
    let digest_arc = Arc::new(digest) as Arc<dyn ToSql + Sync>;
    // Define a macro to create Arc<dyn ToSql + Sync> for each field
    macro_rules! arc_field {
        ($field:expr) => {
            Arc::new($field) as Arc<dyn ToSql + Sync>
        };
    }

    values.extend_from_slice(&[
        node_id_arc,
        arc_field!(m.stream_id),
        arc_field!(m.tor_stream_id),
        arc_field!(m.circuit_id),
        arc_field!(m.filesize_bytes),
        arc_field!(m.payload_progress_100),
        arc_field!(m.payload_progress_80),
        arc_field!(m.time_to_first_byte),
        arc_field!(m.time_to_last_byte),
        arc_field!(m.error_code),
        arc_field!(m.endpoint_local),
        Arc::new(unix_ts_end),
        Arc::new(unix_ts_start),
        arc_field!(m.mbps),
        arc_field!(m.path),
        arc_field!(m.cbt_set),
        arc_field!(m.filtered_out),
        arc_field!(m.is_onion),
        arc_field!(m.middle),
        arc_field!(m.guard),
        arc_field!(m.exit),
        digest_arc,
    ]);
}

fn collect_tor_streams_by_source_port(record: &Value, node_id: &str) -> HashMap<String, Vec<String>> {
    let mut tor_streams_by_source_port: HashMap<String, Vec<String>> = HashMap::new();
    let tor_streams_json = &record["data"][node_id]["tor"]["streams"].to_string();
    let lookup_tor_streams: HashMap<String, Value> =
        serde_json::from_str(&tor_streams_json).expect("Unable to parse circuits JSON");
    let tor_streams: Vec<String> = lookup_tor_streams.keys().cloned().collect();
    for tor_stream in tor_streams {
        let ts = &record["data"][node_id]["tor"]["streams"][tor_stream.clone()];
        if let Some(source) = ts["source"].as_str() {
            let source_parts: Vec<&str> = source.split(':').collect();
            if source_parts.len() > 1 { //
                let stream_source_port = source_parts[1];

                tor_streams_by_source_port.entry(stream_source_port.to_string())
                    .or_insert_with(Vec::new)
                    .push(tor_stream.clone());

            }
        }
    }

    tor_streams_by_source_port
}

async fn insert_measurements(
    record: &Value,
    node_id: &String,
    digest: &String,
    streams: &[String],
) -> Result<Vec<u64>, tokio_postgres::Error> {
    let mut values: Vec<Vec<Arc<dyn ToSql + Sync>>> = Vec::new();
    let tor_streams_by_source_port = collect_tor_streams_by_source_port(record, node_id);

    for stream in streams.iter() {
        let stream_json = &record["data"][node_id]["tgen"]["streams"][stream];

        let mut source_port =  String::new();
        // Extracting the 'local' field and attempting to split it to get the port
        if let Some(local) = stream_json["transport_info"]["local"].as_str() {
            let parts: Vec<&str> = local.split(':').collect();
            if parts.len() > 2 { // Make sure there are enough parts after splitting
                source_port = parts[2].to_string(); // Access the third element (index 2) for the port
            }
        }

        if let Some(tor_streams) = tor_streams_by_source_port.get(&source_port) {
            for tor_stream in tor_streams {
                let ts = &record["data"][node_id]["tor"]["streams"][tor_stream.clone()];
                let circuit = ts["circuit_id"].as_str()
                    .map(|s| s.trim())
                    .unwrap_or("");

                if !stream_json["elapsed_seconds"].is_null() || !stream_json["elapsed_seconds"]["payload_bytes_recv"].is_null() {
                    let ts_end: f64 = ts["unix_ts_end"]
                        .to_string()
                        .parse().unwrap();

                    let unix_ts_end: f64 = stream_json["unix_ts_end"]
                        .to_string()
                        .parse().unwrap();

                    if (ts_end - unix_ts_end).abs() < 150.0 {
                        match Measurement::create(&record, node_id, stream, &tor_stream, &circuit) {
                            Ok(m) => {
                                let mut row_values: Vec<Arc<dyn ToSql + Sync>> = Vec::new();
                                add_measurement_to_batch(&mut row_values, node_id.clone(), digest.to_owned(), m);
                                values.push(row_values);
                            }
                            Err(err) => {
                                eprintln!("Error creating Measurement: {}", err);
                            }
                        }
                    }
                };
            }
        }
    }

    let columns = vec![
        "onionperf_node",
        "stream_id",
        "tor_stream_id",
        "circuit_id",
        "filesize_bytes",
        "payload_progress_100",
        "payload_progress_80",
        "time_to_first_byte",
        "time_to_last_byte",
        "error_code",
        "endpoint_local",
        "unix_ts_end",
        "unix_ts_start",
        "mbps",
        "path",
        "cbt_set",
        "filtered_out",
        "is_onion",
        "middle",
        "guard",
        "exit",
        "onionperf_analysis",
    ];

    insert_values_batch("onionperf_measurement", columns, values).await
}

async fn create_db_connection() -> Result<tokio_postgres::Client, anyhow::Error> {
    // Read the configuration file
    let config_data = fs::read_to_string("config.toml").expect("Failed to read config file");
    let config: Config = toml::from_str(&config_data).expect("Failed to parse config file");

    // Create the database connection string
    let connection_string = format!(
        "host={} dbname={} user={} password='{}' connect_timeout={} keepalives={}",
        config.database.host,
        config.database.dbname,
        config.database.user,
        config.database.password,
        config.database.connect_timeout,
        config.database.keepalives,
    );

    // Connect to the database.
    let (client, connection) =
        tokio_postgres::connect(&connection_string, NoTls)
        .await?;

    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    Ok(client)
}

#[tokio::main]
pub async fn run(path: &str) -> Result<()>  {
    let record = Parse::new(path);
    let node_id = &record.node_id;
    let streams = &record.streams;
    let circuits = &record.circuits;
    let digest = get_analysis_digest(path).await.unwrap();
    let tor_guards = &record.tor_guards;
    let tor_streams = &record.tor_streams;

    let _analysis_results = insert_analysis(path, &record, &digest).await;
    let _streams_results = insert_streams(&record.json, node_id, &digest, streams).await;
    let _circuits_results = insert_circuits(&record.json, node_id, &digest, circuits).await;
    let _tor_guards_results = insert_tor_guards(node_id, &digest, tor_guards).await;
    let _tor_streams_results = insert_tor_streams(&record.json, node_id, &digest, tor_streams).await;
    let _measurements_results = insert_measurements(&record.json, node_id, &digest, streams).await;

    Ok(())
}
