use serde_json::Value;
use std::{collections::HashMap, fs};

#[derive(Debug, Default)]
pub struct Parse {
    pub node_id: String,
    pub path: String,
    pub streams: Vec<String>,
    pub circuits: Vec<String>,
    pub tor_streams: Vec<String>,
    pub tor_guards: serde_json::Value,
    pub measurement_ip: String,
    pub op_type: String,
    pub version: String,
    pub filters: String,
    pub json: serde_json::Value,
}

impl Parse {
    fn read_json(path: &str) -> serde_json::Value {
        let data = fs::read_to_string(path).expect("Unable to read file");
        serde_json::from_str(&data).expect("Unable to parse JSON")
    }

    fn get_node_id(res: &serde_json::Value, node_id: &mut String) -> Option<String> {
        let node = res["data"].to_string();
        let lookup_node: HashMap<String, Value> =
            serde_json::from_str(&node).expect("Unable to parse node JSON");
        node_id.push_str(lookup_node.keys().next().expect("No keys found in node JSON"));

        Some(node_id.to_string())
    }



    pub fn new(path: &str) -> Self {
        let res: serde_json::Value = Self::read_json(path);
        let mut node_id = String::new();
        let node_id = Self::get_node_id(&res, &mut node_id).unwrap();
        let json_circuits = res["data"][&node_id]["tor"]["circuits"].to_string();
        let lookup_circuits: HashMap<String, Value> =
            serde_json::from_str(&json_circuits).expect("Unable to parse circuits JSON");
        let circuits: Vec<String> = lookup_circuits.keys().cloned().collect();
        let json_streams = res["data"][&node_id]["tgen"]["streams"].to_string();
        let lookup_streams: HashMap<String, Value> =
            serde_json::from_str(&json_streams).expect("Unable to parse streams JSON");
        let streams: Vec<String> = lookup_streams.keys().cloned().collect();
        let json_tor_streams = res["data"][&node_id]["tor"]["streams"].to_string();
        let lookup_tor_streams: HashMap<String, Value> =
            serde_json::from_str(&json_tor_streams).expect("Unable to parse tor streams JSON");
        let tor_streams: Vec<String> = lookup_tor_streams.keys().cloned().collect();
        let tor_guards: serde_json::Value = res["data"][&node_id]["tor"]["guards"].clone();
        let measurement_ip = res["data"][&node_id]["measurement_ip"].to_string();
        let op_type = res["data"]["type"].to_string();
        let version = res["data"]["version"].to_string();
        let filters = res["data"]["filters"].to_string();

        Self {
            node_id,
            path: path.to_string(),
            streams,
            circuits,
            tor_streams,
            tor_guards,
            measurement_ip,
            op_type,
            version,
            filters,
            json: res,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static PATH: &str = "tests/data/2021-06-01.op-hk6a.onionperf.analysis.json";

    #[test]
    fn test_reading_json_and_parsing_node_id() {
        let res: serde_json::Value = Parse::read_json(PATH);
        let mut node_id = String::new();
        let node_id = Parse::get_node_id(&res, &mut node_id).unwrap();
        assert_eq!(node_id, "op-hk6a");
    }

    #[test]
    fn test_reading_json_and_circuits() {
        let res: serde_json::Value = Parse::read_json(PATH);
        let streams = res["data"]["op-hk6a"]["tgen"]["streams"].to_string();

        assert!(streams.contains("64079:6:localhost:127.0.0.1:35132:ec2-18-162-44-244.ap-east-1.compute.amazonaws.com:18.162.44.244:443"));
    }
}
